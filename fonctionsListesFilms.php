<?php
        function listbox_crt_genres($genresRow) {
            $i = 0;
            echo '<option value="-1"> Aucun </option>';
            while ($row = $genresRow->fetch()) {
                echo '<option value='.$i.'>'.$row[0].'</option>';
                $i+=1;
	    }
        }
        function listebox_crt_date($dateRow) {
            $i = 0;
            echo '<option value="-1"> Aucune </option>';
            while ($row = $dateRow->fetch()) {
                echo '<option value='.$i.'>'.$row[0].'</option>';
                $i+=1;
            }
        }
        function listebox_crt_realisateurs($realisateurRow){
            $i = 0;
            echo '<option value="-1"> Aucun </option>';
            while ($row = $realisateurRow->fetch()) {
                echo '<option value='.$i.'>'.$row[1].' '.$row[2].'</option>';
                $i+=1;
            }                    
        }
        function listebox_crt_realisateurs1($realisateurRow){
            $i = 0;
            while ($row = $realisateurRow->fetch()) {
                echo '<option value='.$i.'>'.$row[1].' '.$row[2].'</option>';
                $i+=1;
            }                    
        }
        function getRealisateur($req, $indice){
            $i = 0;
            while ($row = $req->fetch()) {
                if($i == $indice){
                    return $row[0];
                }
                $i+=1;
            }                    
        }
        function listebox_crt_titres($titresRow){
            $i = 0;
            while ($row = $titresRow->fetch()) {
                echo '<option value='.$row[1].'>'.$row[0].'</option>';
                $i+=1;
            }
            if($i == 0){
                echo '<option value="-1"> Aucun films dans la base </option>';
            }
        }
        function listebox_crt_pays($paysRow){
            $i = 0;
            while ($row = $paysRow->fetch()) {
                echo '<option value='.$i.'>'.$row[0].'</option>';
                $i+=1;
            }                    
        }
        function getPays($req, $indice){
            $i = 0;
            while ($row = $req->fetch()){
                if($i == $indice){
                    return $row[0];
                }
                $i+=1;
            }
        }
        function recherche($ige, $ida, $ire, $connexion){
            if($ige != '-1'){
                if($ida != '-1'){
                    if($ire != '-1'){
                        echo '<h1>Recherche par genre, annee, et realisateur</h1>';
                        return;
                    }
                    else{
                        echo '<h1>Recherche par genre et date</h1>';
                        rechercheParGenreEtDate($ige, $ida, $connexion);
                        return;
                    }
                }
                if($ire != '-1'){   
                    echo '<h1>Recherche par genre et realisateur</h1>';
                    rechercheParGenreEtRealisateur($ige, $ire, $connexion);
                    return;
                }
                else{
                    echo '<h1>Recherche par genre</h1>';
                    rechercheParGenre($ige, $connexion);
                    return;
                }
            }
            if($ida != '-1'){
                if($ire == '-1' && $ige =='-1'){
                    echo '<h1>Recherche par date</h1>';
                    rechercheParDate($ida, $connexion);
                    return;
                }
            }
            if($ire != '-1'){
                if($ida != '-1' && $ige =='-1'){
                    echo '<h1>Recherche par date et realisateur</h1>';
                    rechercheParDateEtRealisateur($ida, $ire, $connexion);
                    return;
                }
                else{
                    echo '<h1>Recherche par realisateur</h1>';
                    rechercheParRealisateur($ire, $connexion);
                    return;
                }
            }
        }
        function rechercheMotCles($requete){
            echo '
                <h1>Recherche par mots cles</h1>
                <table align="center" id="result">
                <thead>
                <th>Titre Original</th>
                <th>Titre Francais </th>
                <th>Pays </th>
                <th>Duree</th>
                <th>Date</th>
                </thead>
                    ';              
            while ($row = $requete->fetch()) {
                    echo '<tr><td id="titreo">'.$row[0];
                    echo '<td id="titref">'.$row[1];
                    echo '<td id="pays">'.$row[2];
                    echo '<td id="duree">'.$row[3];
                    echo '<td id="date">'.$row[4].'</tr>';
               }
            echo '</table>';            
        }
        function rechercheParDate($ida, $connexion){
            $dates = $connexion->query('select distinct(date) from films order by date');
            $dates->setFetchMode(PDO::FETCH_NUM);
            $dateCherchee ='';
            $i=0;
            while ($row = $dates->fetch()) {
                if($i == $ida){
                    $dateCherchee=$row[0];
                    break;
                }
                $i+=1;
            }
            $recherche = $connexion->query('select titre_original, titre_francais, pays, duree, date from films where date='.$dateCherchee);
            $recherche->setFetchMode(PDO::FETCH_NUM);
            echo '<table align="center" id="result">
                <thead>
                <th>Titre Original</th>
                <th>Titre Francais </th>
                <th>Pays </th>
                <th>Duree</th>
                <th>Date</th>
                </thead>
                    ';              
            while ($row = $recherche->fetch()) {
                    echo '<tr><td id="titreo">'.$row[0];
                    echo '<td id="titref">'.$row[1];
                    echo '<td id="pays">'.$row[2];
                    echo '<td id="duree">'.$row[3];
                    echo '<td id="date">'.$row[4].'</tr>';
               }
            echo '</table>';
        }
        
        function rechercheParGenre($ige, $connexion){
            $genres = $connexion->query('SELECT code_genre, nom_genre from genres');
            $genres->setFetchMode(PDO::FETCH_OBJ);
            $codeGenre ='';
            $i=0;
            while ($row = $genres->fetch()) {
                if($i == $ige){
                    $codeGenre = $row->code_genre;
                    break;
                }
                $i+=1;
            }

            echo '<table align="center" id="result">
                <thead>
                <th>Titre Original</th>
                <th>Titre Francais </th>
                <th>Pays </th>
                <th>Duree</th>
                <th>Date</th>
                </thead>
                    '; 
            $recherche = $connexion->query('select ref_code_film from classification where ref_code_genre='.$codeGenre);
            $recherche->setFetchMode(PDO::FETCH_OBJ);
            while ($row2 = $recherche->fetch()) {
                $nomFilm = $connexion->query('select titre_original, titre_francais, pays, duree, date  from films where code_film ='.$row2->ref_code_film);
                $nomFilm->setFetchMode(PDO::FETCH_OBJ);
                echo '<tr>';
                while($row1 = $nomFilm->fetch()){
                    echo '<td id="titreo">'.$row1->titre_original;
                    echo '<td id="titref">'.$row1->titre_francais;
                    echo '<td id="pays">'.$row1->pays;
                    echo '<td id="duree">'.$row1->duree;
                    echo '<td id="date">'.$row1->date;                    
                }
                echo '</tr>';
            }
            echo '</table>';
        }
        function rechercheParGenreEtDate($ige, $ida,$connexion){
            $genres = $connexion->query('SELECT code_genre from genres');
            $genres->setFetchMode(PDO::FETCH_NUM);
            $dates = $connexion->query('select distinct(date) from films order by date');
            $dates->setFetchMode(PDO::FETCH_NUM);
            $codeGenre ='';
            $date ='';
            $i=0;
            while ($row = $genres->fetch()) {
                if($i == $ige){
                    $codeGenre = $row[0];
                    break;
                }
                $i+=1;
            }
            $i=0;
            while($row = $dates->fetch()) {
                if($i == $ida){
                    $date = $row[0];
                    break;
                }
                $i+=1;
            }
            $recherche = $connexion->query('select distinct(titre_original), titre_francais, pays, duree, date from classification natural join genres,films where date='.$date.' and code_genre='.$codeGenre);
            $recherche->setFetchMode(PDO::FETCH_NUM);
            echo '<table align="center" id="result">
                <thead>
                <th>Titre Original</th>
                <th>Titre Francais </th>
                <th>Pays </th>
                <th>Duree</th>
                <th>Date</th>
                </thead>
                    '; 
            while ($row = $recherche->fetch()) {
                echo '<tr>';
                    echo '<td id="titreo">'.$row[0];
                    echo '<td id="titref">'.$row[1];
                    echo '<td id="pays">'.$row[2];
                    echo '<td id="duree">'.$row[3];
                    echo '<td id="date">'.$row[4];                    
                echo '</tr>';
            }
            echo '</table>';
        }
        function rechercheParDateEtRealisateur($ida, $ire, $connexion){
            //$genres = mysql_query('SELECT code_genre from genres');
            $realisateurs = $connexion->query('select code_indiv, nom, prenom from individus where code_indiv in(select realisateur from films where realisateur in (select code_indiv from individus)) order by nom');
            $realisateurs->setFetchMode(PDO::FETCH_NUM);
            $dates = $connexion->query('select distinct(date) from films order by date');
            $dates->setFetchMode(PDO::FETCH_NUM);
            $realisateur='';
            $date ='';
            $i=0;
            while ($row = $realisateurs->fetch()) {
                if($i == $ire){
                    $realisateur = $row[0];
                    break;
                }
                $i+=1;
            }
            $i=0;
            while($row = $dates->fetch()) {
                if($i == $ida){
                    $date = $row[0];
                    break;
                }
                $i+=1;
            }
            $recherche = $connexion->query('select distinct(titre_original), titre_francais, pays, duree, date from films where realisateur='.$realisateur.' and date='.$date);
            $recherche->setFetchMode(PDO::FETCH_NUM);
            echo '<table align="center" id="result">
                <thead>
                <th>Titre Original</th>
                <th>Titre Francais </th>
                <th>Pays </th>
                <th>Duree</th>
                <th>Date</th>
                </thead>
                    '; 
            while ($row = $recherche->fetch()) {
                echo '<tr>';
                    echo '<td id="titreo">'.$row[0];
                    echo '<td id="titref">'.$row[1];
                    echo '<td id="pays">'.$row[2];
                    echo '<td id="duree">'.$row[3];
                    echo '<td id="date">'.$row[4];                    
                echo '</tr>';
            }
            echo '</table>';
        }
        function rechercheParGenreEtRealisateur($ige, $ire, $connexion){
            $genres = $connexion->query('SELECT code_genre from genres');
            $genres->setFetchMode(PDO::FETCH_NUM);
            $realisateurs = $connexion->query('select code_indiv, nom, prenom from individus where code_indiv in(select realisateur from films where realisateur in (select code_indiv from individus)) order by nom');
            $realisateurs->setFetchMode(PDO::FETCH_NUM);
            $codeGenre ='';
            $realisateur ='';
            $i=0;
            while ($row = $genres->fetch()) {
                if($i == $ige){
                    $codeGenre = $row[0];
                    break;
                }
                $i+=1;
            }
            $i=0;
            while($row = $realisateurs->fetch()) {
                if($i == $ire){
                    $realisateur = $row[0];
                    break;
                }
                $i+=1;
            }
            $recherche = $connexion->query('select distinct(titre_original), titre_francais, pays, duree, date  from classification natural join genres, films where ref_code_genre='.$codeGenre.' and realisateur='.$realisateur);
            $recherche->setFetchMode(PDO::FETCH_NUM);
            echo '<table align="center" id="result">
                <thead>
                <th>Titre Original</th>
                <th>Titre Francais </th>
                <th>Pays </th>
                <th>Duree</th>
                <th>Date</th>
                </thead>
                    '; 
            while ($row = $recherche->fetch()) {
                echo '<tr>';
                    echo '<td id="titreo">'.$row[0];
                    echo '<td id="titref">'.$row[1];
                    echo '<td id="pays">'.$row[2];
                    echo '<td id="duree">'.$row[3];
                    echo '<td id="date">'.$row[4];
                echo '</tr>';
            }
            echo '</table>';
        }
        function rechercheParRealisateur($ire, $connexion){
            $realisateurs = $connexion->query('select code_indiv, nom, prenom from individus where code_indiv in(select realisateur from films where realisateur in (select code_indiv from individus)) order by nom');
            $realisateurs->setFetchMode(PDO::FETCH_NUM);
            $i = 0;
            $codeRea='';
            while($row = $realisateurs->fetch()) {
                if($i == $ire){
                    $codeRea = $row[0];
                    break;
                }
                $i+=1;
            }
            echo '<table align="center" id="result">
                <thead>
                <th>Titre Original</th>
                <th>Titre Francais </th>
                <th>Pays </th>
                <th>Duree</th>
                <th>Date</th>
                </thead>
                    ';             
            $recherche = $connexion->query('select titre_original, titre_francais, pays, duree, date from films where realisateur='.$codeRea);
            $recherche->setFetchMode(PDO::FETCH_NUM);
            echo '<tr>';
            while ($row = $recherche->fetch()) {
                    echo '<td id="titreo">'.$row[0];
                    echo '<td id="titref">'.$row[1];
                    echo '<td id="pays">'.$row[2];
                    echo '<td id="duree">'.$row[3];
                    echo '<td id="date">'.$row[4]; 
                echo '</tr>';
            }
            echo '</table>';
      }

?> 