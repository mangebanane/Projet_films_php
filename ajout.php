<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" type="text/css" href="header.css">
        <title>add</title>
    </head>
    <body>
        <?php
            require './ConnectMysql.php';
            include 'header.php';
            $listePays = $connexion->query('select nom_fr_fr from pays');
            $listeRealisateurs = $connexion->query('select code_indiv, nom, prenom from individus order by nom');
            echo'<form id="formSupr" method="post">
                <div id="ajout_film">
                <h2>Ajouter un film</h2>
                    <table>
                        <tr>
                            <td>Titre original</td>
                            <td><input type="text" name="titre_o"/></td>
                        </tr>
                        <tr>
                            <td>Titre francais</td>
                            <td><input type="text" name="titre_fr"/></td>
                        </tr>
                        <tr>
                            <td>Pays</td>
                            <td><select name="pays" id="pays">', listebox_crt_pays($listePays),'</selected></td>
                        </tr>
                        <!-- select distinct(pays) from films; -->
                        <tr>
                            <td>Date de realisation</td>
                            <td><input type="number" name="ddrea" value="2015"/></td>
                        <tr>
                            <td>Duree (min)</td>
                            <td><input type="number" name="duree" value="120"/></td>
                        </tr>
                            <td>Couleur</td>
                            <td><select name="couleur" id"coul"><option value="couleur">Couleur</option>
                                                                <option value="NB">Noir et Blanc</option>
                                                                <option value="NB/coleur">Les Deux</option></td>
                        <tr>
                            <td>Realisateur</td>
                            <td><select name="rea" id ="rea">',  listebox_crt_realisateurs1($listeRealisateurs),'</selected></td>
                        <!-- mettre image a null -->
                        </tr>
                        <caption><input type="submit" value="Ajouter ce film" name = "addFilm" id="film"></caption>
                    </table>
                </div>
                <h2>Ajouter un genre</h2>
                <div id="creation_genre">
                    <table>
                        <tr>
                            <td>Nom du genre:</td>
                            <td><input type="text" name="nom_genre"/></td>
                            <!-- select max(code_genre) from genre; -->
                        </tr>
                            <caption><input type="submit" value="Ajouter ce genre" name = "addGenre" id="gen"></caption>
                    </table>
                </div>                
                <div id="creation_realisateur">
                <h2>Ajouter un realisateur</h2>    
                    <table>
                    <tr>
                        <td>Nom du realisateur</td>
                        <td><input type="text" name="nom_re"/></td>
                    </tr>
                    <tr>
                        <td>Prenom du realisateur</td>
                        <td><input type="text" name="pre_re"/></td>
                    </tr>
                    <tr>
                        <td>Nationalite</td>
                        <td><input type="text" name="nat_re"/></td>
                    </tr>
                    <tr>
                        <td>Annee de Naissance</td>
                        <td><input type="number" name="ddn_re" value="1970"/></td>
                    </tr>
                    <tr>
                        <td>Annee de Mort</td>
                        <td><input type="number" name="ddm_re" value="2015"/></td>    
                    <caption><input type="submit" value="Ajouter ce realisateur" name = "addRea" id="rea"></caption>
                    </table>
                </div>
                </form>';
        
            if (isset($_REQUEST['addGenre'])) {
                $maxcodegenre = $connexion->query('select max(code_genre) from genres');
                $maxcodegenre->setFetchMode(PDO::FETCH_NUM);
                $max = $maxcodegenre->fetch();
                $max = $max[0]+1;
                if(!empty($_POST['nom_genre'])){
                    try{
                        $requete = 'insert into genres (code_genre, nom_genre) values ("'.$max.'","'.$_POST['nom_genre'].'")';
                     //   echo $requete;
                        $insert = $connexion->query($requete);
                       /* $stmt->bindParam(1, $max1);
                        $stmt->bindParam(2, $nomG);
                        $max1 = $max;
                        $nomG = $_GET['nom_genre'];*/
                        //$stmt->execute(array('max'=>$max, 'name'=>$_GET['nom_genre']));"
                    /*    $stmt->execute(array('32',"bonjour"));*/
                    echo 'Genre : '.$_POST['nom_genre']. ' ajoute avec succes';
                    }catch(PDOException $e){
                        echo $e->getMessage();
                    }
                }
            }
            if (isset($_REQUEST['addFilm'])){
                if(!empty($_POST['titre_o']) && !empty($_POST['titre_fr']) && !empty($_POST['ddrea']) && !empty($_POST['duree']) && !empty($_POST['couleur'])){
                    $listePays = $connexion->query('select nom_fr_fr from pays');
                    $listeRealisateurs = $connexion->query('select code_indiv, nom, prenom from individus order by nom');
                    $pays = getPays($listePays, $_POST['pays']);
                    $realisateur = getRealisateur($listeRealisateurs, $_POST['rea']);
                    $maxCode = $connexion->query('select max(code_film) from films');
                    $codeFilm = $maxCode->fetch()[0]+1;
                    echo $codeFilm;
                    echo $_POST['titre_o'].' '.$_POST['titre_fr'].' '.$pays.' '.$_POST['ddrea'].' '.$_POST['duree'].' '.$_POST['couleur'].' '.$realisateur;
                    $requete = 'insert into films (code_film, titre_original, titre_francais, pays, date, duree, couleur, realisateur, image)
                            values("'.$codeFilm.'","'.$_POST['titre_o'].'","'.$_POST['titre_fr'].'","'.$pays.'","'.$_POST['ddrea'].'","'.$_POST['duree'].'","'.$_POST['couleur'].'","'.$realisateur.'","NULL")';
                    $insert = $connexion->query($requete);
                }
                        
            }
            if (isset($_REQUEST['addRea'])){
                if(!empty($_POST['nom_re']) && !empty($_POST['pre_re']) && !empty($_POST['nat_re']) &&!empty($_POST['ddn_re']) && !empty($_POST['ddm_re'])){
                    $maxCode = $connexion->query('select max(code_indiv) from individus');
                    $codeIndiv = $maxCode->fetch()[0]+1;
                    try{
                        $requete = 'insert into individus(code_indiv, nom, prenom, nationalite, date_naiss, date_mort) '
                            . 'values("'.$codeIndiv.'","'.$_POST['nom_re'].'","'.$_POST['pre_re'].'","'.$_POST['nat_re'].'","'.$_POST['ddn_re'].'","'.$_POST['ddm_re'].'")';
                    echo $requete;$insert = $connexion->query($requete);
                    }catch(PDOException $e){
                        echo $e->getMessage();
                    }
                }
            }
            /*
            if (isset($_REQUEST['searchtitre'])){
                $recherche = '%'.strtolower($_GET['titrecherche']).'%';
                $requete = 'select titre_original, titre_francais, pays, duree, date from films where lower(titre_francais) like \''.$recherche.'\' or lower(titre_original) like \''.$recherche.'\' order by titre_francais';
                $rechercheTitres = $connexion->query($requete);
                $rechercheTitres->setFetchMode(PDO::FETCH_NUM);
                rechercheMotCles($rechercheTitres);
            }*/
            
            ?>
    </body>
</html>
