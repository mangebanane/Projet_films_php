<!DOCTYPE html>
<html>
    <head>
        <title>search</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" type="text/css" href="header.css">
    </head>
    <body>
        <?php
            require './ConnectMysql.php';
            include 'header.php';
                $genres = $connexion->query('SELECT nom_genre from genres');
                $genres->setFetchMode(PDO::FETCH_NUM);
                $dates = $connexion->query('select distinct(date) from films order by date');
                $dates->setFetchMode(PDO::FETCH_NUM);
               // $acteurs = mysql_query('select distinct(ref_code_acteur),nom,prenom from acteurs natural join individus where code_indiv = ref_code_acteur');
                $realisateurs = $connexion->query('select code_indiv, nom, prenom from individus where code_indiv in(select realisateur from films where realisateur in (select code_indiv from individus)) order by nom');
                $realisateurs->setFetchMode(PDO::FETCH_NUM);
                echo '<div id ="haut">
                        <table id="crit">
                            <caption><h1> Recherche par criteres</h1></caption>
                            <tr><td>
                                <form method="post">
                                Genre: 
                                    <select id="bd" name="genres">',  listbox_crt_genres($genres),'</select></td><td>
                                Date sortie: 
                                    <select name="date" id="date">', listebox_crt_date($dates),'</select> </td></tr><tr><td>
                                Realisateur:
                                    <select id="bd" name="realisateur">',  listebox_crt_realisateurs($realisateurs),'</select></td><td>
                                <input type="submit" value="Rechercher" name="search" id="se"/></td>
                            </tr>
                        </table>
                        <div id="parTitre">
                            <h1> Recherche par titre</h1>
                            <input type="text" name="titrecherche"/>
                            <input type="submit" value="Rechercher" name="searchtitre" id="ti"/>
                        </div>
                    </form></div>';
                 
                if (isset($_REQUEST['search'])) {
                    recherche($_POST['genres'], $_POST['date'],$_POST['realisateur'], $connexion);
                }
                if (isset($_REQUEST['searchtitre'])){
                    $recherche = '%'.strtolower($_POST['titrecherche']).'%';
                    $requete = 'select titre_original, titre_francais, pays, duree, date from films where lower(titre_francais) like \''.$recherche.'\' or lower(titre_original) like \''.$recherche.'\' order by titre_francais';
                    $rechercheTitres = $connexion->query($requete);
                    $rechercheTitres->setFetchMode(PDO::FETCH_NUM);
                    rechercheMotCles($rechercheTitres);
                }
                /*
                else{
                    echo "<center><p>Aucun resultat</p></center>";
                }*/
                ?>
        
    </body>
</html>








